package server;

public class App {
    public static final int PORT = 8189;

    public static void main(String[] args) {
        new Server(PORT);
    }
}
