package server;

import javafx.util.converter.LocalDateTimeStringConverter;

import java.io.*;
import java.net.Socket;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class ClientHandler implements Runnable {
    private Socket socket;
    public PrintWriter out;
    private Scanner in;
    private static int clientsCount = 0;
    private String clientName;


    public ClientHandler(Socket s) throws FileNotFoundException {
        try {
            this.socket = s;
            out = new PrintWriter(s.getOutputStream());
            in = new Scanner(s.getInputStream());
            clientsCount++;
            clientName = "Client #" + clientsCount;
        } catch (IOException e) {
        }
    }

    @Override
    public void run() {
        out.write(String.valueOf("Date: " + LocalDate.now()) + "\n");
        out.write(String.valueOf("Time: " + LocalTime.now()) + "\n");
        out.write("Hello my dear File Runner!!!\n\n");
        help();
        System.out.println("Date: " + LocalDate.now() );
        System.out.println("Time: " + LocalTime.now() + "\n");

        out.flush();
        while (true) {
            if (in.hasNext()) { //считывает строку
                String message = in.nextLine(); //записываем ее в переменную message;

                StringTokenizer stringTokenizer = new StringTokenizer(message); //Берем токенайзер и принимаем в него message
                String firstWord = stringTokenizer.nextToken(); //FirstWord = первое слово в message
                try {
                    String secondWord = stringTokenizer.nextToken(); //SecondWord = 2 слово в message,
                    // но может быть так что в сообщении одно слово
                    // и выйдет ошибка NoSuchElementException


                    if (firstWord.equals("download")) {                 //Если первое слово download, то
                        String downloadPath = dirName + "/" + secondWord; // строку downloadPath делаем путем файла, который будем скачивать
                        try {
                            InputStream in = new FileInputStream(downloadPath);
                            OutputStream out = new FileOutputStream("C:/temp/" + secondWord); // скачиваем файл и сохранияем его по этому пути
                            int length = 4096;
                            byte[] buffer = new byte[length];
                            int readed; //возвращает количество записанных байт
                            while ((readed = in.read(buffer)) != -1) {
                                out.write(buffer, 0, readed);
                            }
                            in.close();
                            out.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (firstWord.equals("upload")) { // тут если upload, то все тоже самое что и у download, только в другую сторону
                        String uploadPath = dirName + "/" + secondWord;
                        InputStream in = new FileInputStream("C:/temp/" + secondWord);
                        OutputStream out = new FileOutputStream(uploadPath);
                        int length = 4096;
                        byte[] buffer = new byte[length];
                        int readed; //возвращает количество записанных байт
                        while ((readed = in.read(buffer)) != -1) {
                            out.write(buffer, 0, readed);
                        }
                        in.close();
                        out.close();
                    }
                } catch (NoSuchElementException | IOException e) {
                }

                System.out.println(clientName + ": " + message);
                out.println("ECHO: " + message);
                out.flush();

                if (message.equals("list")) list(new File(dirName), dirPrint);
                if (message.contains("cd <")) {
                    lastPath = path;
                    path = message.replace("cd <", "");
                    path = path.replace(">", "");
                    dirName += "//" + path;
                    dirPrint += " / " + path;
                    out.println("\n" + dirPrint + " - ok" + "\n");
                    out.flush();
                }
                if (message.equals("cd ..")) {
                    if (dirName.equals("C://")) {
                        out.write("Вы не можете подниматься выше каталога C:/");
                    }
                    dirName = dirName.replace("//" + path, "");
                    dirPrint = dirPrint.replace(" / " + path, "");
                    out.println(dirPrint + "\n");
                    out.println(dirName + "\n");
                    out.flush();
                    path = lastPath;
                }

                if (message.equals("help")) {
                    help();
                }

                if (message.equals("exit")) {
                    break;
                }
            }
            else {
                out.write("Такой команды нет, попробуйте снова\n");
               // System.out.println("Server: error catalog not exists");   //to much output process при вылете клиента
                help();
            }
        }
        try {
            System.out.println("Client disconnected");
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void help() {
        out.write("Команды:\n" +
                "list - выводит список всех каталогов и файлов в текущем каталоге\n\n" +
                "cd <Название каталога> - переходит в выбранный каталог. Пример ввода: cd <Catalog>\n\n" +
                "cd .. - переход на вышестоящий каталог, но не выше С:/Users\n\n" +
                "help - выводит список команд\n\n" +
                "exit - завершает работу программы\n\n");
        out.flush();
    }

    String path = "Users", dirName = "C://Users", dirPrint = "C / Users", lastPath = "Users";

    private void list(File dir, String dirPrint) {
        try {
            if (dir.isDirectory()) {
                for (File item : dir.listFiles()) {
                    if (item.isDirectory()) {
                        out.println(dirPrint + " / <" + item.getName() + "> \n");
                        out.flush();
                    } else {
                        out.println(dirPrint + " / " + item.getName() + "\n");
                        out.flush();
                    }
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

}
